# Fancybox

Blaze-Компонент реализующий работу просмотра изображений с помощью  [fancybox](http://fancyapps.com/fancybox/3/).

### Вызов компонета
```spacebars
{{> Fancybox id=galleryId pictures=myArrayOfPicture}}
```
### Параметры
```javascript
const props = {
  // id для объединения фото в галерею [optional]
  id: {
    type: String,
    autoValue() {
      if (!this.isSet) {
        return Random.id();
      }
    }
  },
  // массив изображений
  pictures: {
    type: Array,
    defaultValue: []
  },
  'pictures.$': {
    type: String,
    optional: true,
  },
}
```
