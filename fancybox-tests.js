// Import Tinytest from the tinytest Meteor package.
import { Tinytest } from "meteor/tinytest";

// Import and rename a variable exported by fancybox.js.
import { name as packageName } from "meteor/fancybox";

// Write your tests here!
// Here is an example.
Tinytest.add('fancybox - example', function (test) {
  test.equal(packageName, "fancybox");
});
