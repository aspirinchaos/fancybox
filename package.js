Package.describe({
  name: 'fancybox',
  version: '0.1.1',
  summary: 'Blaze wrapper for fancybox',
  git: 'https://bitbucket.org/aspirinchaos/fancybox.git',
  documentation: 'README.md',
});

Npm.depends({
  '@fancyapps/fancybox': '3.3.5',
});

Package.onUse(function (api) {
  api.versionsFrom('1.5');
  // api.use('ecmascript');
  api.use([
    'ecmascript',
    'tmeasday:check-npm-versions',
    'templating',
    'random',
    'template-controller',
  ]);
  api.addFiles('fancybox.css', 'client');
  api.mainModule('fancybox.js', 'client');
});

Package.onTest(function (api) {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('fancybox');
  api.mainModule('fancybox-tests.js');
});
