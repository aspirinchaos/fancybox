import { TemplateController } from 'meteor/template-controller';
import { checkNpmVersions } from 'meteor/tmeasday:check-npm-versions';
import { Random } from 'meteor/random';
import SimpleSchema from 'simpl-schema';
import '@fancyapps/fancybox';
import './fancybox.html';

checkNpmVersions({ 'simpl-schema': '1.5.3', classnames: '2.2.6' }, 'fancybox');

const Fancybox = TemplateController('Fancybox', {
  props: new SimpleSchema({
    id: {
      type: String,
      autoValue() {
        if (!this.isSet) {
          return Random.id();
        }
      },
    },
    pictures: {
      type: Array,
      defaultValue: [],
    },
    'pictures.$': {
      type: String,
      optional: true,
    },
  }),
  state: {
  },
  onCreated() {
  },
  onRendered() {
  },
  helpers: {
    firstPicture() {
      const [picture] = this.props.pictures;
      return picture || '/no-photo.png';
    },
    pictures() {
      // пропускаем 1 картинку, потому что выводим её отдельно
      const [, ...pictures] = this.props.pictures;
      return pictures;
    },
  },
  events: {
  },
});

export { Fancybox };
